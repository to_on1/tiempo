// Estas dos variables deben ser declaradas fuera del try/catch
// para que puedan ser serradas en el bloque finally
HttpURLConnection urlConnection = null;
BufferedReader reader = null;
// Contendra la respuesta JSON como una cadena
String forecastJsonStr = null;
try {
    // Construyendo al URL para las consultas de OpenWeatherMap,
    // Los posibles parametros están disponibles en la página forecast API en
    // http://openweathermap.org/API#forecast
    URL url = new URL("http://api.openweathermap.org/data/2.5/forecast/daily?q=Cochabamba,bo&units=metric&cnt=7&lang=es");
    // Crear la solicitud a OpenWeatherMap, y abrir la conexión
    urlConnection = (HttpURLConnection) url.openConnection();
    urlConnection.setRequestMethod("GET");
    urlConnection.connect();
    // Read the input stream into a String
    // Lee la secuencia de entrada en una cadena
    InputStream inputStream = urlConnection.getInputStream();
    StringBuffer buffer = new StringBuffer();
    if (inputStream == null) {
        // Nothing to do.
        forecastJsonStr = null;
    }
    reader = new BufferedReader(new InputStreamReader(inputStream));
    String line;
    while ((line = reader.readLine()) != null) {
        // Ya que es JSON, añadir una nueva línea no es necesario (no afectará el análisis)
        // pero hace que la depuración sea mucho más fácil si imprime el búfer completo para la depuración.
        buffer.append(line + "\n");
    }
    if (buffer.length() == 0) {
        // Stream was empty. No point in parsing.
        // Si Stream esta vacío. No hay necesidad de analizarlo.
        forecastJsonStr = null;
    }
    forecastJsonStr = buffer.toString();
} catch (IOException e) {
    Log.e("PlaceholderFragment", "Error ", e);
    // Si el código no consiguió con éxito los datos del tiempo, no hay necesidad de analizarlo.
    forecastJsonStr = null;
} finally{
    if (urlConnection != null) {
        urlConnection.disconnect();
    }
    if (reader != null) {
        try {
            reader.close();
        } catch (final IOException e) {
            Log.e("PlaceholderFragment", "Error closing stream", e);
        }
    }
}
